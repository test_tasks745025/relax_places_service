up:
	docker-compose --env-file ./.env up -d --build

down:
	docker-compose down

migrate:
	docker-compose exec relax_places_api php artisan migrate

reset_migrations:
	docker-compose exec relax_places_api php artisan migrate:reset

dbseed:
	docker-compose exec relax_places_api php artisan db:seed

test:
	docker-compose exec relax_places_api php artisan test