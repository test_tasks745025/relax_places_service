<?php

namespace Tests\Feature;


use App\Models\FavoritePlace;
use App\Models\User;
use App\Utilities\JWTUtilities;
use Database\Seeders\RelaxPlaceSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class FavoritePlaceTest extends TestCase
{
    use RefreshDatabase;

    public function test_show_favorite_places(): void
    {
        $user = User::factory()->create();
        FavoritePlace::factory(FavoritePlace::FAVOURITE_PLACES_LIMIT)->create(['user_id' => $user->id]);

        $response = $this->get("/api/v1/users/$user->id/favorites");
        $response->assertOk()->assertJson(
            fn(AssertableJson $json) => $json->has('data', FavoritePlace::FAVOURITE_PLACES_LIMIT)
                ->etc()
        );
    }

    public function test_add_favorite_place()
    {
        $user = User::factory()->create();
        $token = JWTUtilities::getToken($user);
        $placeID = random_int(1, RelaxPlaceSeeder::RELAX_PLACES_COUNT);
        $response = $this->withToken($token)->post(
            "/api/v1/users/$user->id/favorites",
            [
                'place_id' => $placeID,
            ]
        );

        $expectedMessage = "the place $placeID has been added to your favorite";
        $response->assertStatus(201)->assertJsonPath('message', $expectedMessage);
    }

    public function test_add_favorite_exceed_the_limit()
    {
        $user = User::factory()->create();
        for ($i = 1; $i <= FavoritePlace::FAVOURITE_PLACES_LIMIT; $i++) {
            FavoritePlace::factory()->create(
                [
                    'user_id' => $user->id,
                    'place_id' => $i
                ]
            );
        }


        $token = JWTUtilities::getToken($user);
        $placeID = FavoritePlace::FAVOURITE_PLACES_LIMIT+1;

        $response = $this->withToken($token)->post(
            "/api/v1/users/$user->id/favorites",
            [
                'place_id' => $placeID,
            ]
        );

        $expectedMessage = 'you cannot add more places';
        $response->assertStatus(422)->assertJsonPath('message', $expectedMessage);
    }

    public function test_add_favorite_duplicate()
    {
        $user = User::factory()->create();
        $placeID = random_int(1, FavoritePlace::FAVOURITE_PLACES_LIMIT);
        FavoritePlace::factory()->create(
            [
                'user_id' => $user->id,
                'place_id' => $placeID
            ]
        );
        $token = JWTUtilities::getToken($user);

        $response = $this->withToken($token)->post(
            "/api/v1/users/$user->id/favorites",
            [
                'place_id' => $placeID,
            ]
        );

        $expectedMessage = 'this place is already in your favorite';
        $response->assertStatus(422)->assertJsonPath('message', $expectedMessage);
    }


    public function test_add_favorite_to_other_user()
    {
        $user = User::factory()->create();

        $otherUser = User::factory()->create();
        $token = JWTUtilities::getToken($otherUser);

        $placeID = random_int(1, FavoritePlace::FAVOURITE_PLACES_LIMIT);
        

        $response = $this->withToken($token)->post(
            "/api/v1/users/$user->id/favorites",
            [
                'place_id' => $placeID,
            ]
        );

        $expectedMessage = "you don't have permissions to access this resource";
        $response->assertStatus(403)->assertJsonPath('message', $expectedMessage);
    }
}