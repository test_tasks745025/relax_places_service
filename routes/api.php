<?php

use App\Http\Controllers\RelaxPlaceController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\FavoritePlaceController;
use App\Http\Middleware\JWTMiddleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HealthcheckController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->name('v1')->group(function () {
    Route::get('healthcheck', HealthcheckController::class);

    Route::prefix('places')->name('places')->group(function () {
        Route::get('', [RelaxPlaceController::class, 'index']);
    });

    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);

    Route::prefix('users')->name('users')->group(function () {
        Route::prefix('{userID}/favorites')->name('users')->group(function () {
            Route::get('', [FavoritePlaceController::class, 'index']);
            Route::post('', [FavoritePlaceController::class, 'store'])->middleware([JWTMiddleware::class]);
        });
    });
});