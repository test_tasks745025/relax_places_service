<?php

namespace App\Utilities;

use App\Models\User;
use Firebase\JWT\JWT;

class JWTUtilities
{
    public static function getToken(User $user)
    {
        $payload = [
            'iss' => "relax_places_service",
            'id' => $user->id,
            'iat' => time(),
            'exp' => time() + 60 * 60
        ];

        return JWT::encode($payload, env('JWT_SECRET'), 'HS256');
    }
}