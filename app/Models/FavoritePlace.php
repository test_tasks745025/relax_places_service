<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavoritePlace extends Model
{
    use HasFactory;

    const FAVOURITE_PLACES_LIMIT = 3;
    protected $fillable = [
        'user_id',
        'place_id'
    ];
}