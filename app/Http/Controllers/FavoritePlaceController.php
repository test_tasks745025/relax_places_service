<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFavoritePlaceRequest;
use App\Models\FavoritePlace;
use App\Models\User;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Log;

class FavoritePlaceController extends Controller
{
    public function index($userID)
    {
        $user = User::findOrFail($userID);

        $usersFavouritePlaces = $user->favoritePlaces;

        return response()->json(['data' => $usersFavouritePlaces]);
    }

    public function store(StoreFavoritePlaceRequest $request, $userID)
    {
        $user = $request->input('user');
        $usersPlacesCount = $user->favoritePlaces->count();

        if ($usersPlacesCount >= FavoritePlace::FAVOURITE_PLACES_LIMIT) {
            return response()->json(['message' => 'you cannot add more places'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $placeID = $request['place_id'];

        $favoritePlace = new FavoritePlace();
        $favoritePlace->user_id = $userID;
        $favoritePlace->place_id = $placeID;

        try {
            $favoritePlace->save();
        } catch (QueryException $e) {
            if (str_contains(strtolower($e->getMessage()), 'unique')) {
                 return response()->json(['message' => 'this place is already in your favorite'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }
            Log::error("FavoritePlaceController:error while saving favorite place" . $e->getMessage());
            throw $e;
        }

        return response()->json(['message' => "the place $placeID has been added to your favorite"], Response::HTTP_CREATED);
    }
}