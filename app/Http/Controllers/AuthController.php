<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Utilities\JWTUtilities;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{

    public function register(RegisterRequest $request)
    {
        $user = new User();
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->password = Hash::make($request['password']);
        $user->save();

        return response()->json(['message' => 'successfully registered', 'data' => $user], Response::HTTP_CREATED);
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request['email'])->first();
        if (empty($user)) {
            return response()->json(['error' => 'The user with this email was not found'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (!Hash::check($request['password'], $user->password)) {
            return response()->json(['error' => 'Invalid password specified'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response()->json([
            'message' => 'successfully logged in',
            'data' => $user,
            'token' => JWTUtilities::getToken($user)
        ]);
    }
}