<?php

namespace App\Http\Controllers;

use App\Models\RelaxPlace;

class RelaxPlaceController extends Controller
{
    const PER_PAGE = 10;
    public function index()
    {
        $relaxPlaces = RelaxPlace::paginate(RelaxPlaceController::PER_PAGE);

        return $relaxPlaces;
    }
}