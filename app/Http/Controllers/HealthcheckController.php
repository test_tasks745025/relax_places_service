<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HealthcheckController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request)
    {
        $data = [
            "status" => "available"
        ];

        return response()->json($data);
    }
}