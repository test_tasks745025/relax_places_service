<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class JWTMiddleware
{

    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->bearerToken();
        if (!$token) {
            return response()->json([
                'error' => "Token wasn't provided"
            ], 401);
        }

        try {
            $credentials = JWT::decode($token, new Key(env('JWT_SECRET'), 'HS256'));
        } catch (ExpiredException $e) {
            return response()->json([
                'error' => 'Provided token is expired'
            ], 400);
        } catch (Exception $e) {
            Log::error('JwtMiddleware:error while decoding token:' . $e->getMessage());
            return response()->json([
                'error' => 'An error while decoding token:'
            ], 400);
        }

        $user = User::find($credentials->id);
        $request->merge(['user'=>$user]);

        return $next($request);
    }
}