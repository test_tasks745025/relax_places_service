<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;

class StoreFavoritePlaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        $user=$this->input('user');
        $specifiedID = $this->route('userID');
        return $user->id ==$specifiedID;
    }

    public function rules(): array
    {
        return [
            'place_id' => 'required|integer'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'message' => 'Validation errors',
            'data' => $validator->errors()
        ],Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    public function failedAuthorization()
    {
        throw new HttpResponseException(response()->json([
            'message' => "you don't have permissions to access this resource",
        ],Response::HTTP_FORBIDDEN));
    }
}
