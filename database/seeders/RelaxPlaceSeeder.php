<?php

namespace Database\Seeders;

use App\Models\RelaxPlace;
use Illuminate\Database\Seeder;

class RelaxPlaceSeeder extends Seeder
{
    const RELAX_PLACES_COUNT = 50;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        RelaxPlace::factory()
            ->count(RelaxPlaceSeeder::RELAX_PLACES_COUNT)
            ->create();
    }
}