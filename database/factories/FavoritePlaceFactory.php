<?php

namespace Database\Factories;

use Database\Seeders\RelaxPlaceSeeder;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\FavoritePlace>
 */
class FavoritePlaceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id'=>fake()->unique()->numberBetween(1,9000),
            'place_id'=>fake()->unique()->numberBetween(1,RelaxPlaceSeeder::RELAX_PLACES_COUNT)
        ];
    }
}
